require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  test "full title helper" do
    assert_equal full_title,         'Docker-Rails Sample App'
    assert_equal full_title('Help'), 'Help | Docker-Rails Sample App'
  end
end
