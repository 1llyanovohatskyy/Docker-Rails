# README


## Build the project
Before starting, [install Compose](https://docs.docker.com/compose/install/).
```
docker-compose build
```
If you are running Docker on Linux, the files rails new created are owned by root. This happens because the container runs as the root user. If this is the case, change the ownership of the new files.
```
sudo chown -R $USER:$USER .
```
## Connect the database
The app is now bootable, but you’re not quite there yet. By default, Rails expects a database to be running on `localhost` - so you need to point it at the `db` container instead. You also need to change the database and username to align with the defaults set by the `postgres` image.

Replace the contents of `config/database.yml` with the:
```
default: &default
  adapter: postgresql
  encoding: unicode
  host: db
  username: postgres
  password:
  pool: 5

development:
  <<: *default
  database: toyapp_development


test:
  <<: *default
  database: toyapp_test
```
You can now boot the app with docker-compose up:
```
docker-compose up
```
Finally, you need to create the database. In another terminal, run:
```
docker-compose run web rake db:create
```
## Stop the application
To stop the application, run docker-compose down in your project directory. You can use the same terminal window in which you started the database, or another one where you have access to a command prompt. This is a clean way to stop the application.
```
docker-compose down
```
## Restart the application
To restart the application run `docker-compose up` in the project directory.
## Rebuild the application
If you make changes to the Gemfile or the Compose file to try out some different configurations, you need to rebuild. Some changes require only `docker-compose build`, but a full rebuild requires a re-run of `docker-compose run web bundle install` to sync changes in the `Gemfile.lock` to the host, followed by `docker-compose build`.
