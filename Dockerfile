FROM ruby:2.5
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /toyapp
WORKDIR /toyapp
COPY Gemfile /toyapp/Gemfile
COPY Gemfile.lock /toyapp/Gemfile.lock
RUN bundle install
COPY . /toyapp
